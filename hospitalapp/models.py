from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)

    class Meta(object):
        abstract = True

class Admin(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='admin')


    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='admin')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

class Patient(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True, blank=True)
    photo = models.ImageField(upload_to='patient', null=True, blank=True)
    latitude = models.FloatField(max_length=50,null=True,blank=True)
    longitude = models.FloatField(max_length=50,null=True,blank=True)


    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='patient')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)



class Hospital(TimeStamp):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to='hospitalimage')
    slogan = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    profile_image = models.ImageField(
        upload_to='hospitalimage', null=True, blank=True)
    profile_video = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField()
    alt_email = models.EmailField(null=True, blank=True)
    mission_vision = models.TextField()
    about = models.TextField()
    privacy_policy = models.TextField()
    terms_and_condition = models.TextField()
    show_popup = models.BooleanField()
    popup_title = models.CharField(max_length=200, null=True, blank=True)
    popup_content = models.TextField(null=True, blank=True)
    popup_action = models.CharField(max_length=200, null=True, blank=True)
    facebook = models.CharField(max_length=200)
    twitter = models.CharField(max_length=200)
    youtube = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Slider(TimeStamp):
    title = models.CharField(max_length=200)
    caption = models.CharField(max_length=200)
    image = models.ImageField(upload_to='sliderimage')
    action = models.CharField(max_length=200)
    action_link = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Department(TimeStamp):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    description = models.TextField()
    image = models.ImageField(upload_to='departments')

    def __str__(self):
        return self.title


class Doctor(TimeStamp):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    speciality = models.CharField(max_length=200)
    qualification = models.CharField(max_length=200)
    image = models.ImageField(upload_to='doctor')
    email = models.EmailField()
    experience = models.PositiveIntegerField()
    about = models.TextField()
    latitude = models.FloatField(max_length=50,null=True,blank=True)
    longitude = models.FloatField(max_length=50,null=True,blank=True)
    checkcovid = models.BooleanField(null=True,blank=True,default=False)
    # longs = models.FloatField()
    # lats = models.FloatField()
    

    def __str__(self):
        return self.name


DAYS = (
    ("Sunday", "Sunday"),
    ("Monday", "Monday"),
    ("Tuesday", "Tuesday"),
    ("Wednesday", "Wednesday"),
    ("Thursday", "Thursday"),
    ("Friday", "Friday"),
    ("Saturday", "Saturday"),

)


class Schedule(TimeStamp):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    day = models.CharField(max_length=50, choices=DAYS)
    arrival = models.TimeField()
    departure = models.TimeField()

    def __str__(self):
        return self.doctor.name 


GENDER = (
    ('Male', 'Male'),
    ('Female', 'Female'),
)


class Appointment(TimeStamp):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    gender = models.CharField(max_length=200, choices=GENDER)
    mobile = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    date = models.DateField()
    time = models.TimeField()
    message = models.TextField(null=True, blank=True)
    urgent = models.BooleanField(default=False)


class Contact(TimeStamp):
    name=models.CharField(max_length=200)
    email=models.EmailField()
    phone=models.CharField(max_length=200)
    address=models.CharField(max_length=200)
    message=models.TextField()

    def __str__(self):
      return self.name

class Covid(TimeStamp):
    BreathingProblem=models.BooleanField("Breathing Problem", default=False)
    fever=models.BooleanField("fever", default=False)
    DryCough=models.BooleanField("Dry Cough", default=False)
    Sorethroat=models.BooleanField("Sorethroat", default=False)
    HyperTension=models.BooleanField("Hyper Tension", default=False)
    Abroadtravel=models.BooleanField("Abroad Travel", default=False)
    ContactwithCOVIDPatient=models.BooleanField("Contact with COVID Patient", default=False)
    Attended_Large_Gathering=models.BooleanField("Attended Large Gathering", default=False)
    Visited_Public_Exposed_Places=models.BooleanField("Visited Public Exposed Places	", default=False)
    Family_working_in_Public_Places=models.BooleanField("Family working in Public Exposed Places", default=False)
    
    # def __str__(self):
    #     return self.name


    Pregencies = models.IntegerField("Pregencies")
    Glucose = models.IntegerField("Glucose")
    BloodPressure = models.IntegerField("BloodPressure")
    SkinThickness = models.IntegerField("SkinThickness")
    Insulin = models.IntegerField("Insulin")
    BMI = models.FloatField("BMI")
    Age = models.IntegerField("Age")
    DiabetesPedigreeFunction = models.FloatField("DiabetesPedigreeFunction")
