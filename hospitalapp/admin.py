from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register([Hospital, Slider, Department,Covid,
                     Doctor, Schedule, Appointment,Contact,Patient])
