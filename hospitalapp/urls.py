from django.urls import path
from .views import *

app_name = 'hospitalapp'
urlpatterns = [
    # clients
#     path('', HomeView.as_view(), name="index"),
    path('', HomeView, name="index"),
    path('recommend-doctor/', RecommendDoctorView.as_view(), name="recommend"),
    path('cliendhome', ClientHomeView.as_view(), name="clienthome"),
    path('my-loc/', ClientLocationView.as_view(), name="clientlocation"),
    path('doctor/', ClientDoctorView.as_view(), name="clientdoctor"),
    path('search/', ClientSearchView.as_view(), name="clientsearch"),
    path('my-appointment/doctor/', ClientAppointementShowView.as_view(), name="clientappointmentlist"),
    path('appointment/', ClientAppointmentCreateView.as_view(), name="clientappointment"),
    path('makeappointment/<int:id>/', MakeAppointment, name="makeappointment"),
    path('saveappointment/<int:id>/', saveAppointment, name="saveappointment"), 
    path('contact/', ClientContactView.as_view(), name="clientcontact"),
    path('login/', ClientLoginView.as_view(), name="clientlogin"),
    path('registration/', ClientRegistrationView.as_view(), name="clientregistration"),
    path('logout/', ClientLogoutView.as_view(), name="logout"),




    # admin
    path('hospital-admin/', AdminHomeView.as_view(), name="adminhome"),
    path('hospital-admin/table/', AdminTableView.as_view(), name="admintable"),
    path('hospital-admin/slider/list/',
         AdminSliderListView.as_view(), name="adminsliderlist"),
    path('hospital-admin/slider/create/',
         AdminSliderCreateView.as_view(), name="adminslidercreate"),
    path('hospital-admin/slider/<int:pk>/update/',
         AdminSliderUpdateView.as_view(), name="adminsliderupdate"),
    path("hospital-admin/slider/<int:pk>/delete/",
         AdminSliderDeleteView.as_view(), name="adminsliderdelete"),
    path('hospital-admin/doctor/list/',
         AdminDoctorListView.as_view(), name="admindoctorlist"),
    path("hospital-admin/doctor/create",
         AdminDoctorCreateView.as_view(), name="admindoctorcreate"),
    path('hospital-admin/doctor/<int:pk>/update/',
         AdminDoctorUpdateView.as_view(), name="admindoctorupdate"),
    path("hospital-admin/doctor/<int:pk>/delete/",
         AdminDoctorDeleteView.as_view(), name="admindoctordelete"),
    path('hospital-admin/login/', AdminLoginView.as_view(), name="adminlogin"),
    path('hospital-admin/logout/', AdminLogoutView.as_view(), name="adminlogout"),
    path('hospital-admin/register/', AdminRegistrationView.as_view(), name="adminregistration"),
    path('hospital-admin/appointment/list/',
         AdminAppointmentView.as_view(), name="adminappointment"),
     path('hospital-admin/appointment/list-urgent/',
         AdminAppointmentUrgentView.as_view(), name="adminappointmenturgent"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
