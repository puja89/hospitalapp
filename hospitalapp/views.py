from os import name
from django.shortcuts import render, redirect
from django.views.generic import *
from pandas.core import indexing
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils import timezone
from django.conf import settings
from django.conf.urls.static import static
import pandas as pd
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
import pickle
import folium
from folium.plugins import MarkerCluster
import numpy as np
from geopy.distance import geodesic


class UserRequiredMixing(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.username == "admin":
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)

class ClientRegistrationView(CreateView):
    template_name = "clienttemplates/registration.html"
    form_class = PatientRegistrationForm
    success_url = reverse_lazy('hospitalapp:clientlogin')


    def form_valid(self, form):
        a = form.cleaned_data['username']
        b = form.cleaned_data['password']
        c = form.cleaned_data['email']
        puja_user = User.objects.create_user(a, c, b)
        form.instance.user = puja_user
        # form.instance.interests=puja_user.interests
        return super().form_valid(form)

class ClientLoginView(FormView):
    template_name = "clienttemplates/login.html"
    form_class = LoginForm
    success_url = reverse_lazy("hospitalapp:index")

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]

        user = authenticate(username=uname, password=pword)

        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "clienttemplates/login.html", {
                "form": form,
                "error": "Invalid username or password"
            })

        return super().form_valid(form)

class ClientLogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')    


class ClientMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['loginform'] = LoginForm
        return context

class ClientHomeView(TemplateView,ClientMixin,UserRequiredMixing):
    template_name = 'clienttemplates/clienthome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = AppointmentForm
        context['sliders'] = Slider.objects.all()
        # context['doctors'] = Doctor.objects.all()

        return context
        

# class HomeView(CreateView):
#     template_name = 'clienttemplates/home.html'
#     form_class = CovidForm
#     success_url = reverse_lazy("hospitalapp:recommend")

#     def form_valid(self, form):
#         print(form.cleaned_data)
#         return super().form_valid(form)
    
#     # send form data to success url as context

def HomeView(request):
    
    if request.user.is_authenticated:
        form = CovidForm()
        if request.method == 'POST':
            print("this is after hits",request.POST)
            form = CovidForm(request.POST)
            if form.is_valid:
                form.save()
                item_dist = {}
                print(form.cleaned_data.values())
                for key,val in form.cleaned_data.items():
                    item_dist[key] = [val]
                data =  pd.DataFrame.from_dict(item_dist)
                data.replace({False: 0, True: 1}, inplace=True)
                covid_data = data[['BreathingProblem', 'fever', 'DryCough', 'Sorethroat', 'HyperTension','Abroadtravel','ContactwithCOVIDPatient','Attended_Large_Gathering','Visited_Public_Exposed_Places','Family_working_in_Public_Places']]
                diab_data = data[["Pregencies","Glucose","BloodPressure","SkinThickness","Insulin","BMI","Age","DiabetesPedigreeFunction"]]
                loaded_model = pickle.load(open('logisticreg.pkl', 'rb'))
                diab_GNB_model = pickle.load(open('GNB.pkl', 'rb'))
                diab_result = diab_GNB_model.predict(diab_data)
                print(loaded_model)
                print("model loaded")
                res = loaded_model.predict(covid_data)
                print("this is covid result",res)
                print("this is diab result",diab_result)
                results = ''
                doctor_df = pd.DataFrame(list(Doctor.objects.all().values()))
                print("before =>",doctor_df)
                if res[0] == 0:
                    context = {'result':"Negative"}
                    results = 'N'
                    
                else:
                    context = {'result':"Positive"}
                    results = 'P'
                    # doctor_df = pd.DataFrame(list(Doctor.objects.all().values()))
                    doctor_df = doctor_df.loc[doctor_df['checkcovid'] == True]
                    print("After =>",doctor_df)

                
                myData = pd.DataFrame(list(Patient.objects.filter(user=request.user.id).values()))
                
                print("mydata",myData.latitude)
                # origin = (30.172705, 31.526725)  # (latitude, longitude) don't confuse
                # dist = (30.288281, 31.732326)
                la = float(myData.latitude)
                lo = float(myData.longitude)
                dist = (la, lo)
                # dist = (request.user.lats,request.user.longs)  #yo chio pachi user ko longs ra lat model ma update vaye pachi
                
                '''#yo tala ko code lai uncomment gara hai pull gare ra model update gare pachi'''
                # queryset = list(Doctor.objects.all().order_by('avg_rating').values())
                # doctors = list(Doctor.objects.all().values())
                # limit which fields
                #this is for map 
                geo_code = []
                geo_label = []
                print("this is result",res[0])
                # if res[0] == 0:
                #     doctor_df = pd.DataFrame(list(Doctor.objects.all().values()))
                #     # doctor_df = pd.DataFrame(list(Doctor.objects.filter(checkcovid=True).values()))
                #     print("this is coming in df")
                # if res[0] ==1:
                #     doctor_df = pd.DataFrame(list(Doctor.objects.filter(checkcovid=True).values()))
                #     # doctor_df = pd.DataFrame(list(Doctor.objects.all().values()))

                # print(doctor_df)
                # if results == 'P':
                #     doctor_df = doctor_df.loc[doctor_df['checkcovid'] == True]
                # print("after filter",doctor_df)
                distances =[]
                for index, x in doctor_df.iterrows():
                    distance = geodesic(((x['latitude']),x['longitude']), dist).kilometers #adding destination for of each doctor
                    distances.append(distance)
                print("distances",distances)
                doctor_df['dist'] = distances
                doctor_df2 = doctor_df
                doctor_df2 = doctor_df2.sort_values(by='dist')
                for index, x in doctor_df2.tail().iterrows():
                    geo_label.append(x['name'])
                    geo_code.append([x['latitude'],x['longitude']])
                doctors = doctor_df2.tail()
                print("this is docs",doctors)
                docs_details = doctors[['id','name','about','dist']].to_dict('records') 
                context['doctors'] = docs_details
                print("context doctor",context['doctors'])
                long=0
                lats=0
                for x in geo_code:
                    long=long+x[0]
                    lats=lats+x[1]
                long = long/len(geo_code)
                lats = lats/len(geo_code)
                m = folium.Map(location=[np.mean(long), np.mean(lats)], zoom_start=6)

                marker_cluster = MarkerCluster().add_to(m)

                for i in range(len(geo_code)):
                    folium.Marker(
                        location=list(geo_code[i]),
                        popup=geo_label[i],
                        icon=folium.Icon(color="green", icon="ok-sign"),
                    ).add_to(marker_cluster)  
                folium.PolyLine(geo_code, color='red').add_to(m)      
                
                # folium.Marker(
                #         location=list([la,lo]),
                #         popup="Your location",
                #         icon=folium.Icon(color="red", icon="ok-sign"),
                #     ).add_to(marker_cluster)

                m=m._repr_html_()
                context['my_map']=m
                if diab_result[0] == 0:
                    context['diab']="Negative"
                else:
                    context['diab']="Positive"
                print("log long",request.user.id)
                return  render(request,'clienttemplates/recommendation.html',context)
        context = {'form':form}
        return render(request,'clienttemplates/home.html',context)
    else:
        return redirect("/login/")

def CovidTest(request):
    if request.user.is_authenticated:
        form = CovidForm()
        if request.method == 'POST':
            form = CovidForm(request.POST)
            if form.is_valid:
                form.save()
                context = {'form':form}
                return  render(request,'clienttemplates/recommendation.html',context)
        context = {'form':form}
        return render(request,'clienttemplates/home.html',context)
    else:
        return redirect("/login/")




class RecommendDoctorView(TemplateView):
    template_name = 'clienttemplates/recommendation.html'



class ClientLocationView(TemplateView):
    template_name = "clienttemplates/mylocation.html"


class ClientAppointementShowView(ListView):
    template_name = "clienttemplates/clientappointmentshow.html"
    model = Appointment
    context = "allappoinments"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            print(self.request.user.username)
            context["allappoinments"] = Appointment.objects.filter(name=self.request.user.username)
         
        return context
    

class ClientDoctorView(ListView):
    template_name = "clienttemplates/clientdoctor.html"
    model = Doctor
    # context['doctors']=Doctor.objects.all()
    context_object_name = 'doctors'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['departments'] = Department.objects.all()
        context['doctors'] = Doctor.objects.all()
        if self.request.GET:
            department = self.request.GET.get('department')
            name = self.request.GET.get('name')
            context['doctors'] = Doctor.objects.filter(
                department__title=department, name__contains=name)
        return context


class ClientSearchView(TemplateView):
    template_name = "clienttemplates/clientsearch.html"

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         kw = self.request.GET["keyword"]
#         # print(kw,"*****---")
#         results = Doctor.objects.filter(
#             Q(name__contains=kw) | Q(department__name__contains=kw))
#         context["results"] = results

#         return context


class ClientAppointmentCreateView(CreateView):
    template_name = 'clienttemplates/clientappointment.html'
    form_class = AppointmentForm 
    success_url = reverse_lazy("hospitalapp:clienthome")

def MakeAppointment(request,id):
    
    if request.method == 'POST':
        deoartment = Department.objects.get(id=1)
        d = Doctor.objects.get(id=id)
        name = request.POST['name']
        address = request.POST['address']
        gender = request.POST['gender']
        mobile = request.POST['mobile']
        email = request.POST['email']
        date = request.POST['date']
        time = request.POST['time']
        message = request.POST['message']
        urgent = request.POST['urgent']

        form = Appointment(department=deoartment,urgent=urgent,doctor=d,name=name,address=address,gender=gender,mobile=mobile,email=email,date=date,time=time,message=message)
        form.save()
        return redirect('/')

    context={}
    return render(request,'clienttemplates/appointdoctor.html',context)

def saveAppointment(request,id):
    print("make appointment")
    print("form", request.name)
    context={}
    return render(request,'clienttemplates/success.html',context)


class ClientContactView(CreateView):
    template_name = "clienttemplates/clientcontact.html"
    form_class = ContactForm
    success_url = reverse_lazy("hospitalapp:clienthome")


class AdminHomeView(ClientMixin,TemplateView):
    template_name = 'admintemplates/adminhome.html'


class AdminTableView(TemplateView):
    template_name = 'admintemplates/admintable.html'


class AdminSliderListView(ListView):
    template_name = "admintemplates/adminsliderlist.html"
    queryset = Slider.objects.all()
    context_object_name = "sliderlist"


class AdminSliderCreateView(CreateView):
    template_name = "admintemplates/adminslidercreate.html"
    form_class = SliderForm
    success_url = reverse_lazy("hospitalapp:adminsliderlist")


class AdminSliderUpdateView(UpdateView):
    template_name = "admintemplates/adminslidercreate.html"
    form_class = SliderForm
    model = Slider
    success_url = reverse_lazy("hospitalapp:adminsliderlist")


class AdminSliderDeleteView(DeleteView):
    template_name = "admintemplates/adminsliderdelete.html"
    model = Slider
    success_url = reverse_lazy("hospitalapp:adminsliderlist")

class AdminDoctorListView(ListView):
    template_name = "admintemplates/admindoctorlist.html"
    queryset = Doctor.objects.all()
    context_object_name = "doctorsall"


class AdminDoctorCreateView(CreateView):
    template_name = "admintemplates/admindoctorcreate.html"
    form_class = DoctorForm
    success_url = reverse_lazy("hospitalapp:admindoctorlist")

class AdminDoctorUpdateView(UpdateView):
    template_name = "admintemplates/admindoctorcreate.html"
    form_class = DoctorForm
    model = Doctor
    success_url = reverse_lazy("hospitalapp:admindoctorlist")


class AdminDoctorDeleteView(DeleteView):
    template_name = "admintemplates/admindoctordelete.html"
    model = Doctor
    success_url = reverse_lazy("hospitalapp:admindoctorlist")


class AdminLoginView(FormView):
    template_name="admintemplates/adminlogin.html"
    form_class=LoginForm
    success_url=reverse_lazy("hospitalapp:adminhome")

    def form_valid(self,form):
        uname=form.cleaned_data["username"]
        pword=form.cleaned_data["password"]
        user=authenticate(username=uname,password=pword)

        if user is not None:
            login(self.request,user)
        else:
            return render(self.request,"adminlogin.html", {
                "form": form,
                "error": "Invalid username or password"
            })

        return super().form_valid(form)  

class AdminLogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/login/")          


class AdminRegistrationView(FormView):
    template_name="admintemplates/adminregistration.html"
    form_class=RegistrationForm
    success_url=reverse_lazy("hospitalapp:adminlogin.html")

    def form_valid(self, form):
        a = form.cleaned_data["username"]
        b = form.cleaned_data["email"]
        c = form.cleaned_data["password"]

        User.objects.create_user(a, b, c)

        return super().form_valid(form)

class AdminAppointmentView(ListView):
    template_name = "admintemplates/adminappointment.html"
    model = Appointment
    context = "allappoinments"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            doctors = Doctor.objects.get(name=self.request.user.username)
            context["allappoinments"] = Appointment.objects.filter(doctor=doctors.id)
        return context

class AdminAppointmentUrgentView(ListView):
    template_name = "admintemplates/adminappointmenturgent.html"
    model = Appointment
    context="allappoinments"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            doctors = Doctor.objects.get(name=self.request.user.username)
            context["allappoinments"] = Appointment.objects.filter(doctor=doctors.id,urgent=True)
           
        return context
