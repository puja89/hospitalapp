from django import forms
from django.forms import widgets
from .models import *
from django.contrib.auth.models import User
import re


class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        # fields="__all__"
        exclude = ["is_active"]
        widgets = {
            'date': forms.DateInput(attrs={
                'type': 'date',
                'class': 'form-control date'
            }),
            'time': forms.TimeInput(attrs={
                'type': 'time',
                'class':'form-control timepicker'

            }),
            'urgent':  forms.CheckboxInput(attrs={ 'class': 'form-control','placeholder': 'Urgent'}),
            'name': forms.Select(attrs={'placeholder': 'Name', 'class': 'form-control dropdown-menu '}),
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'mobile': forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'form-control'}),
            'address': forms.TextInput(attrs={'placeholder': 'Address', 'class': 'form-control'}),

        }


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('name', 'email', 'phone', 'address', 'message')
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'form-control'}),
            'address': forms.TextInput(attrs={'placeholder': 'Address', 'class': 'form-control'}),
            'message': forms.Textarea(attrs={'placeholder': 'Message ', 'class': 'form-control'}),
        }


class SliderForm(forms.ModelForm):
    class Meta:
        model = Slider
        # fields = "__all__"
        exclude = ['is_active']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs['class'] = "form-control"
            field.widget.attrs['placeholder'] = "Enter " + name + "..."


class DoctorForm(forms.ModelForm):
    class Meta:
        model = Doctor
        fields = ('name', 'department', 'speciality', 'qualification',
                  'image', 'email', 'experience', 'about'
                  )
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name..', 'class': 'form-control'}),
            'department': forms.Select(attrs={'placeholder': 'department..', 'class': 'form-control'}),
            'speciality': forms.TextInput(attrs={'placeholder': 'speciality..', 'class': 'form-control'}),
            'qualification': forms.TextInput(attrs={'placeholder': 'qualification..', 'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'placeholder': 'email..', 'class': 'form-control'}),
            'experience': forms.TextInput(attrs={'placeholder': 'experience..', 'class': 'form-control'}),
            'about': forms.Textarea(attrs={'placeholder': 'about...', 'class': 'form-control'}),
           

        }


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter username'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter password'
    }))


class RegistrationForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter username'
    }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter email'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter password'
    }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter password'
    }))
    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError(
                "user with this username already exists")

        return uname

    def clean_confirm_password(self):
        p1 = self.cleaned_data["password"]
        p2 = self.cleaned_data["confirm_password"]
        if not re.findall('\d', p1):
            raise forms.ValidationError("password must contain alphanumeric")
        if len(p1) < 8 or len(p2) < 8:
            raise forms.ValidationError("password is short")
        if p1 != p2:
            raise forms.ValidationError("password didnot match")

        return p2


class PatientRegistrationForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control"}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control"}))
    email = forms.CharField(widget=forms.EmailInput(attrs={"class":"form-control"}))
    mobile = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    address = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    latitude = forms.FloatField(widget=forms.TextInput(attrs={"class":"form-control"}))
    longitude = forms.FloatField(widget=forms.TextInput(attrs={"class":"form-control"}))

    class Meta:
        model = Patient
        fields = ['username', 'email', 'password', 'confirm_password',
                  'mobile', 'name', 'address', 'photo','latitude','longitude']

    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError(
                "user with this username already exists")

        return uname

    def clean_confirm_password(self):
        p1 = self.cleaned_data["password"]
        p2 = self.cleaned_data["confirm_password"]
        if not re.findall('\d', p1):
            raise forms.ValidationError("password must contain alphanumeric")
        if len(p1) < 8 or len(p2) < 8:
            raise forms.ValidationError("password is short")
        if p1 != p2:
            raise forms.ValidationError("password didnot match")

        return p2

class CovidForm(forms.ModelForm):
    class Meta:
        model = Covid
        fields = ('BreathingProblem', 'fever', 'DryCough', 'Sorethroat', 'HyperTension','Abroadtravel','ContactwithCOVIDPatient','Attended_Large_Gathering','Visited_Public_Exposed_Places','Family_working_in_Public_Places',
        "Pregencies","Glucose","BloodPressure","SkinThickness","Insulin","BMI","Age","DiabetesPedigreeFunction")
        
        widget = {
            'BreathingProblem': forms.CheckboxInput(attrs={'class':'form-control'})
        }
       